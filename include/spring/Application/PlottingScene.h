#pragma once

#include <spring\Framework\IScene.h>
#include <qobject.h>
#include <qcombobox.h>
#include <qcustomplot.h>

namespace Spring
{
	class PlottingScene : public IScene
	{
		Q_OBJECT

	public:
		PlottingScene(const std::string& ac_szSceneName);

		void createScene() override;

		void release() override;

		void currentIndexChanged(int index);

		~PlottingScene();



  public slots:
      void mp_BackButton();
      void mp_OnPlotButton();
	  void mp_ComboChaged();
	
  private:
    void mp_InitPlotters();

		QWidget * centralWidget;
    QCustomPlot* mv_customPlotTime;
    QCustomPlot* mv_customPlotFreq;
    QComboBox* mv_signalComboBox;
	};
}
