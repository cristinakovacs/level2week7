#pragma once
#include <spring\Framework\IApplicationModel.h>
#include <spring\Application\global.h>
#include <spring\Application\InitialScene.h>
#include <spring\Application\BaseScene.h>
#include <spring\Application\PlottingScene.h>
#include<spring\Application\SinsScene.h>

namespace Spring
{
	class Application_EXPORT_IMPORT_API ApplicationModel : public IApplicationModel
	{
	public:

		ApplicationModel();

		virtual void defineScene();

		virtual void defineInitialScene();

		virtual void defineTransientData();

	private:
		std::shared_ptr<InitialScene> m_initialScene;
		std::shared_ptr<BaseScene>    m_secondScene;
		std::shared_ptr<PlottingScene> m_thirdScene;
		std::shared_ptr<SinsScene> m_forthScene;

	};
}
