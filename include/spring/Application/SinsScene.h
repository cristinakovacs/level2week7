#pragma once

#include <spring\Framework\IScene.h>
#include <qobject.h>
#include <qcustomplot.h>

namespace Spring
{
	class SinsScene : public IScene
	{
		Q_OBJECT

	public:
		SinsScene(const std::string& ac_szSceneName);

		void createScene() override;

		void release() override;

		~SinsScene();



	public slots:
		void mf_BackButton();

	private:

		QWidget * centralWidget;
		QLineEdit * firstLine;
		QLineEdit * secondLine; 
		QLineEdit * thirdLine; 
		QLineEdit * forthLine; 
		QLineEdit * fifthLine;
		QPushButton *pushButton;
		//QGridLayout gridLay;

	public:
		static QString first, second, third, forth, fifth;
	};
}
