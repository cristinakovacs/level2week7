#include <spring/Application/SinsScene.h>
#include "ui_base_scene.h"
#include<spring/Application/BaseScene.h>

namespace Spring
{

	SinsScene::SinsScene(const std::string& ac_szSceneName) : IScene(ac_szSceneName)
	{
	}
	//int stringToInt(QLineEdit* line) {
	//	QString word = line->text();
	//	int number = word.toInt();
	//	return number;
	//}

	void SinsScene::createScene()
	{
		//taking into consideration that I've used baseScene as the base for my scene, let me use the ctor from
		//..BasseScene.cpp; this way, I just have to remove or add some elements 
		&BaseScene::createScene;

		//create the UI
		auto ui = std::make_unique<Ui_baseScene>();
		ui->setupUi(m_uMainWindow.get());

		//adding the new text boxes for the frequency
		ui->gridLayout_2->addWidget(firstLine);
		ui->gridLayout_2->addWidget(secondLine);
		ui->gridLayout_2->addWidget(thirdLine);
		ui->gridLayout_2->addWidget(forthLine);
		ui->gridLayout_2->addWidget(fifthLine);
	

		centralWidget = ui->centralwidget;
		//removinf or adding buttons in the 3rd grid - we don't want to have problems wiht the functionalities
		ui->gridLayout_3->removeWidget(ui->startButton);
		ui->gridLayout_3->removeWidget(ui->stopButton);
		ui->gridLayout_3->removeWidget(ui->backButton);
		ui->gridLayout_3->addWidget(pushButton);
		//et the title of the window
		m_uMainWindow->setWindowTitle(QString("Sins Scene"));
		
		//connect btn's release signal to defined slot
		QObject::connect(pushButton, SIGNAL(released()), this, SLOT(mf_BackButton()));
		
	}

	void SinsScene::release()
	{
		delete centralWidget;
	}

	SinsScene::~SinsScene()
	{

	}

	void SinsScene::mf_BackButton()
	{
		/*SinsScene::first = stringToInt(firstLine);
		SinsScene::second =stringToInt(secondLine);
		SinsScene::third =stringToInt(thirdLine);
		SinsScene::forth =stringToInt(forthLine);
		SinsScene::fifth = stringToInt(fifthLine);*/

		//there are 2 ways to keep the result, and I chose the one with QString.
		//Otherwise, you may replace QString in SinsScene.h with int, and use the first method from this .cpp
		SinsScene::first = firstLine->text();
		SinsScene::second = secondLine->text();
		SinsScene::third = thirdLine->text();
		SinsScene::forth = forthLine->text();
		SinsScene::fifth = fifthLine->text();
		const std::string c_szNextSceneName = "Plotting scene";
		emit SceneChange(c_szNextSceneName);
	}

	
}
